## WWW'22 HRCF: Enhancing Collaborative Filtering via Hyperbolic Geometric Regularization


Authors: Menglin Yang, Min Zhou, Jiahong Liu, Defu Lian, Irwin King

Note: this repository is built upon [HGCF](https://github.com/layer6ai-labs/HGCF) and [HGCN](https://github.com/HazyResearch/hgcn).
 
<a name="Environment"/>

## Environment:
The code was developed and tested on the following python environment:
```
python 3.7.7
pytorch 1.5.1
scikit-learn 0.23.2
numpy 1.20.2
scipy 1.6.2
tqdm 4.60.0
```
<a name="instructions"/>

## Instructions:

Train and evaluation HRCF:

- To evaluate HRCF on Amazon_CD 
  - `bash ./example/run_cd.sh`
- To evaluate HRCF on Amazon_Book
   - `bash ./example/run_book.sh`
- To evaluate HRCF on Yelp
    - `bash ./example/run_yelp.sh`

<a name="citation"/>

## Citation

If you find this code useful in your research, please cite the following paper:

    @inproceedings{yang2022hrcf,
      title={HRCF: Enhancing Collaborative Filtering via Hyperbolic Geometric Regularization},
      author={Menglin Yang, Min Zhou, Jiahong Liu, Defu Lian, Irwin King},
      booktitle={Proceedings of the International World Wide Web Conference},
      year={2022}
    }


